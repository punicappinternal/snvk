package com.punicapp.vk;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.punicapp.sncore.ISNConnector;
import com.punicapp.sncore.SNAccessToken;
import com.punicapp.sncore.SNType;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import io.reactivex.Observer;
import io.reactivex.subjects.PublishSubject;

public class VkSNConnector implements ISNConnector {
    public static final String PUBLIC_PROFILE = null;
    private final int appId;
    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken != null) {

            }
        }
    };
    private Observer<SNAccessToken> observer;
    VKCallback<VKAccessToken> vkCallback = new VKCallback<VKAccessToken>() {
        @Override
        public void onResult(VKAccessToken res) {
            observer.onNext(new SNAccessToken(res.accessToken, res.secret, res.userId));
            observer.onComplete();
        }

        @Override
        public void onError(VKError error) {
            if (error.errorCode == VKError.VK_CANCELED) {
                observer.onError(new Throwable("Cancel"));
            } else {
                observer.onError(new Throwable(error.errorMessage));
            }
            observer.onComplete();
        }
    };

    public VkSNConnector(Context context, int appId) {
        this.appId = appId;
        vkAccessTokenTracker.startTracking();
        VKSdk.customInitialize(context.getApplicationContext(), appId, null);
        VKSdk.initialize(context.getApplicationContext());
    }

    public SNAccessToken getAccessToken() {
        VKAccessToken token = VKAccessToken.currentToken();
        return new SNAccessToken(token.accessToken, token.secret, token.userId);
    }

    @Override
    public SNType getType() {
        return SNType.VK;
    }

    protected boolean isLoggedIn() {
        VKAccessToken currentAccessToken = VKAccessToken.currentToken();
        return currentAccessToken != null;
    }

    @Override
    public boolean requestAuth(Activity shadowActivity) {
        if (isLoggedIn()) {
            VKAccessToken token = VKAccessToken.currentToken();
            observer.onNext(new SNAccessToken(token.accessToken, token.secret, token.userId));
            observer.onComplete();
            return true;
        }
        VKSdk.login(shadowActivity, PUBLIC_PROFILE);
        return false;
    }

    @Override
    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        VKSdk.onActivityResult(requestCode, resultCode, data, vkCallback);
    }

    @Override
    public void bindAuthCallback(PublishSubject<SNAccessToken> observer) {
        this.observer = observer;
    }
}
