package com.punicapp.snvk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.punicapp.sncore.SNAccessToken;
import com.punicapp.sncore.SNManager;
import com.punicapp.sncore.SNType;
import com.punicapp.vk.VkSNConnector;
import com.vk.sdk.util.VKUtil;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DefaultObserver;

public class MainActivity extends AppCompatActivity {

    private SNManager sManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SNManager.Builder builder = new SNManager.Builder(this);
        printVkHashKey();
        sManager = builder.with(new VkSNConnector(this, getResources().getInteger((R.integer.com_vk_sdk_AppId))))
                .build();

        findViewById(R.id.login_vk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Observable<SNAccessToken> auth = sManager.auth(SNType.VK);
                auth.subscribe(new DefaultObserver<SNAccessToken>() {

                    @Override
                    public void onNext(@NonNull SNAccessToken snAccessToken) {
                        showDialog(snAccessToken.toString());
                        cancel();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        showDialog("ERRROR: " + e.getMessage());
                        cancel();
                    }

                    @Override
                    public void onComplete() {
                        cancel();
                    }
                });
            }
        });


    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(message)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    public void printVkHashKey() {
        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        Log.i("WAT", "printVkHashKey() Hash Key: " + fingerprints);
    }


}
