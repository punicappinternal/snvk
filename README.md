# gradle-simple

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/snVk.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/snVk)

Punicapp VK module library, which is used in internal projects.

To install the library add: 
 
```
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:snVk:1.0.1'
   }
```